/**
 * A property type for a property.
 */
export interface IPropertyType {
  /**
   * The property type id.
   */
  id: string;

  /**
   * The human readable unique code.
   */
  code: 'textarea' | 'decimal' | 'integer' | 'text';

  /**
   * The property type name.
   */
  name: string;

  /**
   * The property type description.
   */
  description: string;
}
