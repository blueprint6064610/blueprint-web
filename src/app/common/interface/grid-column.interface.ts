/**
 * A column configuration for the grid.
 */
export interface IGridColumn {
  /**
   * The display text.
   */
  headerText: string;

  /**
   * The field to read of the row.
   */
  field: string;
}
