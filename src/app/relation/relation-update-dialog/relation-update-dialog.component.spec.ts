import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RelationUpdateDialogComponent } from './relation-update-dialog.component';

describe('RelationUpdateDialogComponent', () => {
  let component: RelationUpdateDialogComponent;
  let fixture: ComponentFixture<RelationUpdateDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RelationUpdateDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelationUpdateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
