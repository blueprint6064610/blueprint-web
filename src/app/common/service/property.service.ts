import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { QueryService } from './query.service';
import { IPropertyCreateRequest } from '../interface/property-create-request.interface';
import { IProperty } from '../interface/property.interface';
import { IPropertyUpdateRequest } from '../interface/property-update-request.interface';
import { IQueryRequest } from '../interface/query-request.interface';
import { IQueryResponse } from '../interface/query-response.interface';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {
  constructor(
    private readonly httpClient: HttpClient,
    private readonly queryService: QueryService
  ) { }

  create(schematicId: string, request: IPropertyCreateRequest): Observable<void> {
    const url = PropertyService.apiUrl(schematicId);
    return this.httpClient.post<void>(url, request);
  }

  delete(schematicId: string, propertyId: string): Observable<void> {
    const url = `${PropertyService.apiUrl(schematicId)}/${propertyId}`;
    return this.httpClient.delete<void>(url);
  }

  findBySchematic(schematicId: string, query: IQueryRequest): Observable<IQueryResponse<IProperty>> {
    const url = `${PropertyService.apiUrl(schematicId)}`;
    const params = this.queryService.getParams(query);
    return this.httpClient.get<IQueryResponse<IProperty>>(url, { params });
  }

  update(schematicId: string, propertyId: string, request: IPropertyUpdateRequest): Observable<void> {
    const url = `${PropertyService.apiUrl(schematicId)}/${propertyId}`;
    return this.httpClient.put<void>(url, request);
  }

  private static apiUrl(schematicId: string): string {
    return `${environment.apiBase}/schematics/${schematicId}/properties`;
  }
}
