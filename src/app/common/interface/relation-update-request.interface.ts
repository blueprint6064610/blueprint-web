/**
 * Update a relation.
 */
export interface IRelationUpdateRequest {
  /**
   * The new relation name.
   */
  name: string;

  /**
   * The new relation description.
   */
  description: string;
}
