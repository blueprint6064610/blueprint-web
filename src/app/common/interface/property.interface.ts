import { IPropertyType } from "./property-type.interface";

/**
 * A schematic property.
 */
export interface IProperty {
  /**
   * The property id.
   */
  id: string;

  /**
   * The property name.
   */
  name: string;

  /**
   * The property description.
   */
  description: string;

  /**
   * The property data type.
   */
  propertyType: IPropertyType;
}
