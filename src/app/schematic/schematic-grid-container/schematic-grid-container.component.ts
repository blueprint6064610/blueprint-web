import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { SchematicService } from 'src/app/common/service/schematic.service';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
import { SchematicGridComponent } from '../schematic-grid/schematic-grid.component';

@Component({
  selector: 'app-schematic-grid-container',
  templateUrl: './schematic-grid-container.component.html',
  styleUrls: ['./schematic-grid-container.component.scss']
})
export class SchematicGridContainerComponent {
  @ViewChild('grid') grid?: SchematicGridComponent;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly matDialog: MatDialog,
    private readonly router: Router,
    private readonly schematicService: SchematicService
  ) { }

  get isOneSelected(): boolean {
    return this.grid?.selected.length === 1;
  }

  async create(): Promise<void> {
    await this.router.navigate(['create'], {relativeTo: this.activatedRoute});
  }

  async update(): Promise<void> {
    const id: string | undefined = this.grid?.selected[0]?.id;
    await this.router.navigate([id], {relativeTo: this.activatedRoute});
  }

  delete(): void {
    const schematic = this.grid?.selected[0];
    if (!schematic) { return; }

    this.matDialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Delete Schematic',
        message: 'Are you sure you want to delete this schematic?',
        action: 'Delete',
        theme: 'warn'
      }
    }).afterClosed().subscribe((confirm: boolean) => {
      if (!confirm) { return; }
      this.schematicService.delete(schematic.id).subscribe(() => {
        window.location.reload();
      });
    });
  }
}
