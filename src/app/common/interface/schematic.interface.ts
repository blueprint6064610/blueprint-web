import { IProperty } from "./property.interface";

/**
 * A user defined data structure.
 */
export interface ISchematic {
  /**
   * The schematic id.
   */
  id: string;

  /**
   * The schematic name.
   */
  name: string;

  /**
   * The schematic description.
   */
  description: string;

  /**
   * The schematic properties.
   */
  properties: IProperty[];
}
