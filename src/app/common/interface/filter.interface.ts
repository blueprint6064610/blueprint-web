/**
 * Specifies filtering parameters.
 */
export interface IFilter {
  /**
   * The key and value to filter with.
   */
  [key: string]: string;
}
