import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomCreateContainerComponent } from './custom-create-container.component';

describe('CustomCreateContainerComponent', () => {
  let component: CustomCreateContainerComponent;
  let fixture: ComponentFixture<CustomCreateContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomCreateContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomCreateContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
