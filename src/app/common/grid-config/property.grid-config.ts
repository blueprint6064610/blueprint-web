import { Injector } from "@angular/core";
import { PropertyDataSource } from "../data-source/property.data-source";
import { IGridConfig } from "../interface/grid-config.interface";
import { IProperty } from "../interface/property.interface";
import { PropertyService } from "../service/property.service";

/**
 * Configuration class for a property grid.
 */
export class PropertyGridConfig implements IGridConfig<IProperty> {
  /**
   * @inheritdoc
   */
  columns = [
    { field: 'name', headerText: 'Name' },
    { field: 'description', headerText: 'Description' }
  ];

  /**
   * @inheritdoc
   */
  dataSource = new PropertyDataSource(this.injector.get(PropertyService), this.schematicId);

  /**
   * @inheritdoc
   */
  select = true;

  /**
   * PropertyGridConfig constructor.
   * @param injector The injector.
   * @param schematicId The schematic id.
   */
  constructor(
    private readonly injector: Injector,
    private readonly schematicId: string
  ) { }

  /**
   * @inheritdoc
   */
  getId(row: IProperty): any {
    return row.id;
  }
}
