import { ServerDataSource } from "../data-source/server.data-source";
import { IGridColumn } from "./grid-column.interface";

/**
 * The configuration for the data grid.
 */
export interface IGridConfig<T> {
  /**
   * The grids columns.
   */
  columns: IGridColumn[];

  /**
   * The grids data source.
   */
  dataSource: ServerDataSource<T>;

  /**
   * If items can be selected.
   */
  select: boolean;

  /**
   * Returns a value that uniquely identifies the row.
   * @param row The row.
   */
  getId(row: T): any;
}
