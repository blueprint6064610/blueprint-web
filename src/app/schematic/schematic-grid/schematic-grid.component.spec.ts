import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchematicGridComponent } from './schematic-grid.component';

describe('SchematicGridComponent', () => {
  let component: SchematicGridComponent;
  let fixture: ComponentFixture<SchematicGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchematicGridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchematicGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
