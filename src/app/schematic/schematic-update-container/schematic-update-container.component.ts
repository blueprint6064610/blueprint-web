import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ISchematicUpdateRequest } from 'src/app/common/interface/schematic-update-request.interface';
import { ISchematic } from 'src/app/common/interface/schematic.interface';
import { SchematicService } from 'src/app/common/service/schematic.service';
import { SchematicFormComponent } from '../schematic-form/schematic-form.component';

@Component({
  selector: 'app-schematic-update-container',
  templateUrl: './schematic-update-container.component.html',
  styleUrls: ['./schematic-update-container.component.scss']
})
export class SchematicUpdateContainerComponent implements OnInit {
  @ViewChild(SchematicFormComponent) schematicForm!: SchematicFormComponent;

  schematic!: ISchematic;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly schematicService: SchematicService
  ) { }

  ngOnInit(): void {
    this.schematic = this.activatedRoute.snapshot.data['schematic'];
  }

  get form(): FormGroup {
    return this.schematicForm.schematicForm;
  }

  update(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    const value: ISchematicUpdateRequest = this.form.value;
    const id = this.schematic.id;
    this.schematicService.update(id, value).subscribe(async () => {
      await this.router.navigate(['..'], {relativeTo: this.activatedRoute});
    });
  }
}
