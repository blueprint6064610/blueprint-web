import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { IUser } from 'src/app/common/interface/user.interface';

export function confirmValidator(controlName: string, confirmControlName: string): ValidatorFn {
  return (abstractControl: AbstractControl) => {
    const formGroup = abstractControl as FormGroup;
    const control = formGroup.controls[controlName];
    const confirmControl = formGroup.controls[confirmControlName];
    if (confirmControl.errors && !confirmControl.errors['confirmValidator']) {
      return null;
    }

    if (control.value !== confirmControl.value) {
      return { confirmValidator: true };
    } else {
      return null;
    }
  }
}

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  @Input() user?: IUser;

  userForm = this.formBuilder.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required],
    confirmPassword: ['', Validators.required]
  }, {
    validators: confirmValidator('password', 'confirmPassword')
  });

  constructor(
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    if (this.user) {
      const { firstName, lastName, email } = this.user;
      this.userForm.patchValue({ firstName, lastName, email });
    }
  }
}
