import { Observable } from "rxjs";
import { ServerDataSource } from "./server.data-source";
import { PropertyService } from "../service/property.service";
import { IProperty } from "../interface/property.interface";
import { IQueryRequest } from "../interface/query-request.interface";
import { IQueryResponse } from "../interface/query-response.interface";

/**
 * A data source for querying properties.
 */
export class PropertyDataSource extends ServerDataSource<IProperty> {
  /**
   * PropertyDataSource constructor.
   * @param propertyService The property service.
   * @param schematicId The schematic id.
   */
  constructor(
    private readonly propertyService: PropertyService,
    private readonly schematicId: string,
  ) {
    super();
  }

  /**
   * @inheritdoc
   */
  protected override dataSource$(query: IQueryRequest): Observable<IQueryResponse<IProperty>> {
    return this.propertyService.findBySchematic(this.schematicId, query);
  }
}
