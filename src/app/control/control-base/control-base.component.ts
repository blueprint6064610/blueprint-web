import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { IProperty } from 'src/app/common/interface/property.interface';

@Component({
  selector: 'app-control-base',
  templateUrl: './control-base.component.html',
  styleUrls: ['./control-base.component.scss']
})
export class ControlBaseComponent {
  @Input() control!: FormControl;

  @Input() property!: IProperty;

  get name(): string {
    return this.property.name;
  }
}
