import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { INavigation } from 'src/app/common/interface/navigation.interface';
import { AuthService } from 'src/app/common/service/auth.service';
import { SchematicService } from 'src/app/common/service/schematic.service';

/**
 * A component for the main app frame.
 */
@Component({
  selector: 'app-app-shell',
  templateUrl: './app-shell.component.html',
  styleUrls: ['./app-shell.component.scss']
})
export class AppShellComponent implements OnInit {
  /**
   * The pre-defined navigations.
   */
  navigations: INavigation[] = [];

  /**
   * AppShellComponent constructor.
   */
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly schematicService: SchematicService
  ) { }

  /**
   * @inheritdoc
   */
  ngOnInit(): void {
    this.schematicService.find({ pagination: { page: 0, pageSize: 100 } })
      .subscribe(res => {
        this.navigations = res.items.map(item => ({ icon: 'dynamic_form', label: item.name, route: item.id }));
      });
  }

  /**
   * Navigates to the login page.
   */
  async logout(): Promise<void> {
    this.authService.logout();
    await this.router.navigate(['login']);
  }
}
