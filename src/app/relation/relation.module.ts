import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RelationGridComponent } from './relation-grid/relation-grid.component';
import { GridModule } from '../grid/grid.module';
import { RelationGridContainerComponent } from './relation-grid-container/relation-grid-container.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RelationFormComponent } from './relation-form/relation-form.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { ItemSelectorModule } from '../item-selector/item-selector.module';
import { RelationCreateDialogComponent } from './relation-create-dialog/relation-create-dialog.component';
import { RelationUpdateDialogComponent } from './relation-update-dialog/relation-update-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  declarations: [
    RelationGridComponent,
    RelationGridContainerComponent,
    RelationFormComponent,
    RelationCreateDialogComponent,
    RelationUpdateDialogComponent
  ],
  imports: [
    CommonModule,
    GridModule,
    ItemSelectorModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatToolbarModule,
    ReactiveFormsModule
  ],
  exports: [
    RelationGridContainerComponent
  ]
})
export class RelationModule { }
