import { Component } from '@angular/core';
import { ControlBaseComponent } from '../control-base/control-base.component';

@Component({
  selector: 'app-control-decimal',
  templateUrl: './control-decimal.component.html',
  styleUrls: ['./control-decimal.component.scss']
})
export class ControlDecimalComponent extends ControlBaseComponent { }
