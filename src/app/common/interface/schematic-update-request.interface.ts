/**
 * The schematic update request.
 */
export interface ISchematicUpdateRequest {
  /**
   * The updated schematic name.
   */
  name: string;

  /**
   * The updated schematic description.
   */
  description: string;
}
