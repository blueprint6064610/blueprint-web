import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IProperty } from 'src/app/common/interface/property.interface';

@Component({
  selector: 'app-property-form',
  templateUrl: './property-form.component.html',
  styleUrls: ['./property-form.component.scss']
})
export class PropertyFormComponent implements OnInit {
  @Input() property?: IProperty;

  propertyForm = this.formBuilder.group({
    name: ['', Validators.required],
    description: [''],
    propertyType: [null, Validators.required]
  });

  constructor(
    private readonly formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    if (this.property) {
      const formData = {
        name: this.property.name,
        description: this.property.description,
        propertyType: this.property.propertyType
      };

      this.propertyForm.setValue(formData);
      this.propertyForm.get('propertyType')?.disable();
    }
  }
}
