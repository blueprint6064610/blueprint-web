import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchematicResolver } from '../common/resolver/schematic.resolver';
import { AppShellComponent } from './app-shell/app-shell.component';

/**
 * The AppShellModule routes.
 */
const routes: Routes = [
  {
    path: '',
    component: AppShellComponent,
    children: [
      {
        path: 'settings',
        loadChildren: () => import('../settings/settings.module').then(m => m.SettingsModule)
      },
      {
        path: ':schematicId',
        loadChildren: () => import('../custom/custom.module').then(m => m.CustomModule),
        resolve: { schematic: SchematicResolver }
      }
    ]
  }
];

/**
 * Class AppShellRoutingModule.
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppShellRoutingModule { }
