/**
 * Update user request.
 */
export interface IUserUpdateRequest {
  /**
   * The users updated first name.
   */
  firstName: string;

  /**
   * The users updated last name.
   */
  lastName: string;

  /**
   * The users updated email address.
   */
  email: string;

  /**
   * The users updated password.
   */
  password: string;
}
