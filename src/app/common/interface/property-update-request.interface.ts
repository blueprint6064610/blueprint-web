/**
 * An update property request.
 */
export interface IPropertyUpdateRequest {
  /**
   * The propertys new name.
   */
  name: string;

  /**
   * The propertys new description.
   */
  description?: string;
}
