import { Component, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IUserCreateRequest } from 'src/app/common/interface/user-create-request.interface';
import { UserService } from 'src/app/common/service/user.service';
import { UserFormComponent } from '../user-form/user-form.component';

@Component({
  selector: 'app-user-create-container',
  templateUrl: './user-create-container.component.html',
  styleUrls: ['./user-create-container.component.scss']
})
export class UserCreateContainerComponent {
  @ViewChild(UserFormComponent) userForm!: UserFormComponent;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly userService: UserService
  ) { }

  get form(): FormGroup {
    return this.userForm.userForm;
  }

  create(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    const { firstName, lastName, email, password } = this.form.value;
    const request: IUserCreateRequest = { firstName, lastName, email, password };
    this.userService.create(request).subscribe(async () => {
      await this.router.navigate(['..'], {relativeTo: this.activatedRoute});
    });
  }
}
