import { Component, Inject, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IPropertyUpdateRequest } from 'src/app/common/interface/property-update-request.interface';
import { IProperty } from 'src/app/common/interface/property.interface';
import { PropertyService } from 'src/app/common/service/property.service';
import { PropertyFormComponent } from '../property-form/property-form.component';

export interface IPropertyUpdateDialogData {
  property: IProperty;
  schematicId: string;
}

@Component({
  selector: 'app-property-update-dialog',
  templateUrl: './property-update-dialog.component.html',
  styleUrls: ['./property-update-dialog.component.scss']
})
export class PropertyUpdateDialogComponent {
  @ViewChild(PropertyFormComponent) propertyForm!: PropertyFormComponent;

  constructor(
    @Inject(MAT_DIALOG_DATA) private readonly data: IPropertyUpdateDialogData,
    private readonly matDialogRef: MatDialogRef<PropertyUpdateDialogComponent>,
    private readonly propertyService: PropertyService
  ) { }

  get form(): FormGroup {
    return this.propertyForm.propertyForm;
  }

  get property(): IProperty {
    return this.data.property;
  }

  get name(): string {
    return this.form.get('name')?.value;
  }

  get description(): string {
    return this.form.get('description')?.value;
  }

  update(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    const schematicId = this.data.schematicId;
    const propertyId = this.property.id;
    const value: IPropertyUpdateRequest = {
      name: this.name,
      description: this.description
    };


    this.propertyService.update(schematicId, propertyId, value).subscribe(() => {
      this.matDialogRef.close(true);
    });
  }
}
