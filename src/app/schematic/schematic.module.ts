import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchematicGridContainerComponent } from './schematic-grid-container/schematic-grid-container.component';
import { SchematicGridComponent } from './schematic-grid/schematic-grid.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { GridModule } from '../grid/grid.module';
import { SchematicRoutingModule } from './schematic-routing.module';
import { SchematicFormComponent } from './schematic-form/schematic-form.component';
import { SchematicCreateContainerComponent } from './schematic-create-container/schematic-create-container.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SchematicUpdateContainerComponent } from './schematic-update-container/schematic-update-container.component';
import { PropertyModule } from '../property/property.module';
import { RelationModule } from '../relation/relation.module';

@NgModule({
  declarations: [
    SchematicGridContainerComponent,
    SchematicGridComponent,
    SchematicFormComponent,
    SchematicCreateContainerComponent,
    SchematicUpdateContainerComponent
  ],
  imports: [
    CommonModule,
    GridModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    PropertyModule,
    ReactiveFormsModule,
    RelationModule,
    SchematicRoutingModule
  ]
})
export class SchematicModule { }
