import { Injector } from "@angular/core";
import { UserDataSource } from "../data-source/user.data-source";
import { IGridConfig } from "../interface/grid-config.interface";
import { IUser } from "../interface/user.interface";
import { UserService } from "../service/user.service";

/**
 * Configuration class for a user grid.
 */
export class UserGridConfig implements IGridConfig<IUser> {
  /**
   * @inheritdoc
   */
  columns = [
    { field: 'firstName', headerText: 'First Name' },
    { field: 'lastName', headerText: 'Last Name' },
    { field: 'email', headerText: 'Email' }
  ];

  /**
   * @inheritdoc
   */
  dataSource = new UserDataSource(this.injector.get(UserService));

  /**
   * @inheritdoc
   */
  select = true;
  
  /**
   * UserGridConfig constructor.
   * @param injector The injector.
   */
  constructor(
    private readonly injector: Injector
  ) { }
  
  /**
   * @inheritdoc
   */
  getId(row: IUser): any {
    return row.id;
  }
}
