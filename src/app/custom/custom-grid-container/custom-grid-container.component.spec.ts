import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomGridContainerComponent } from './custom-grid-container.component';

describe('CustomGridContainerComponent', () => {
  let component: CustomGridContainerComponent;
  let fixture: ComponentFixture<CustomGridContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomGridContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomGridContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
