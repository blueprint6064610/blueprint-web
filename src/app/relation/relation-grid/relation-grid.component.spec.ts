import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RelationGridComponent } from './relation-grid.component';

describe('RelationGridComponent', () => {
  let component: RelationGridComponent;
  let fixture: ComponentFixture<RelationGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RelationGridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelationGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
