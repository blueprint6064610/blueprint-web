/**
 * A response interface for any queryable set.
 */
export interface IQueryResponse<T> {
  /**
   * The total number of items found in the query, excluding pagination.
   */
  total: number;

  /**
   * The items for the current page.
   */
  items: T[];
}
