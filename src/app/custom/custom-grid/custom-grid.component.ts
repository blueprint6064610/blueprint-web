import { Component, Injector, Input, OnInit } from '@angular/core';
import { CustomGridConfig } from 'src/app/common/grid-config/custom.grid-config';
import { ICustomEntity } from 'src/app/common/interface/custom-entity.interface';
import { IGridConfig } from 'src/app/common/interface/grid-config.interface';
import { ISchematic } from 'src/app/common/interface/schematic.interface';

@Component({
  selector: 'app-custom-grid',
  templateUrl: './custom-grid.component.html',
  styleUrls: ['./custom-grid.component.scss']
})
export class CustomGridComponent implements OnInit {
  @Input() schematic!: ISchematic;

  gridConfig!: IGridConfig<ICustomEntity>;

  private _selected: ICustomEntity[] = [];

  constructor(
    private readonly injector: Injector
  ) { }

  get selected(): ICustomEntity[] {
    return this._selected;
  }

  ngOnInit(): void {
    this.gridConfig = new CustomGridConfig(this.injector, this.schematic);
  }

  onSelectedChange(event: ICustomEntity[]): void {
    this._selected = event;
  }
}
