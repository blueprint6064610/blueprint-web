import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AppShellRoutingModule } from './app-shell-routing.module';
import { AppShellComponent } from './app-shell/app-shell.component';

/**
 * Class AppShellModule.
 */
@NgModule({
  declarations: [
    AppShellComponent
  ],
  imports: [
    AppShellRoutingModule,
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule
  ]
})
export class AppShellModule { }
