import { Injectable, Injector } from '@angular/core';
import { IItemSelectorConfig } from '../interface/item-selector-config.interface';
import { PropertyTypeItemSelectorConfig } from '../item-selector-config/property-type.item-selector-config';
import { SchematicItemSelectorConfig } from '../item-selector-config/schematic.item-selector-config';

type ItemSelectorConfigConstructor<T> = new (injector: Injector) => IItemSelectorConfig<T>;

/**
 * Class ItemSelectorService.
 */
@Injectable({
  providedIn: 'root'
})
export class ItemSelectorService {
  /**
   * The registered configurations.
   */
  private readonly configurations = new Map<string, ItemSelectorConfigConstructor<any>>();

  /**
   * ItemSelectorService constructor.
   * @param injector The injector.
   */
  constructor(
    private readonly injector: Injector
  ) {
    this.setDefaultConfigs();
  }

  /**
   * Registers the item selector configuration.
   * @param key The key to register the configuration for.
   * @param config The item selector configuration class.
   */
  set<T>(key: string, config: ItemSelectorConfigConstructor<T>): void {
    if (!this.configurations.has(key)) {
      this.configurations.set(key, config);
    }
  }

  /**
   * Gets the item selector configuration.
   * @param key The key to get the configuration for.
   * @returns The item selector configuration.
   */
  get<T>(key: string): IItemSelectorConfig<T> {
    if (!this.configurations.has(key)) {
      throw new Error(`No item selector configuration for ${key}`);
    }

    const ItemSelectorConfig = this.configurations.get(key)!;
    return new ItemSelectorConfig(this.injector);
  }

  /**
   * Sets the default configurations required by the system.
   */
  private setDefaultConfigs(): void {
    this.configurations.set('property-type', PropertyTypeItemSelectorConfig);
    this.configurations.set('schematic', SchematicItemSelectorConfig);
  }
}
