import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ILoginRequest } from '../interface/login-request.interface';
import { ILoginResponse } from '../interface/login-response.interface';

/**
 * The authentication service.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private static readonly API_URL = `${environment.apiBase}/auth`;

  constructor(
    private readonly httpClient: HttpClient
  ) { }

  login(request: ILoginRequest): Observable<ILoginResponse> {
    const url = `${AuthService.API_URL}/login`;
    return this.httpClient.post<ILoginResponse>(url, request).pipe(
      tap(res => localStorage.setItem('token', res.token))
    );
  }

  logout(): void {
    localStorage.removeItem('token');
  }
}
