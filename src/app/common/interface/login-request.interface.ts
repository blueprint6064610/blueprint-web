/**
 * The login request.
 */
export interface ILoginRequest {
  /**
   * The users email.
   */
  email: string;

  /**
   * The users password.
   */
  password: string;
}
