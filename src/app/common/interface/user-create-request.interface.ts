/**
 * Create a new user.
 */
export interface IUserCreateRequest {
  /**
   * The users first name.
   */
  firstName: string;

  /**
   * The users last name.
   */
  lastName: string;

  /**
   * The users email address.
   */
  email: string;

  /**
   * The users password.
   */
  password: string;
}
