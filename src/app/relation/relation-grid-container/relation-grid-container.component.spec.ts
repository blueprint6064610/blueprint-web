import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RelationGridContainerComponent } from './relation-grid-container.component';

describe('RelationGridContainerComponent', () => {
  let component: RelationGridContainerComponent;
  let fixture: ComponentFixture<RelationGridContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RelationGridContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelationGridContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
