import { IItemSelectorConfig } from "../interface/item-selector-config.interface";
import { IPropertyType } from "../interface/property-type.interface";

export class PropertyTypeItemSelectorConfig implements IItemSelectorConfig<IPropertyType> {
  /**
   * @inheritdoc
   */
  getLabel(value: IPropertyType): string {
    return value.name;
  }
}
