import { Component, Injector, Input, OnInit } from '@angular/core';
import { PropertyGridConfig } from 'src/app/common/grid-config/property.grid-config';
import { IGridConfig } from 'src/app/common/interface/grid-config.interface';
import { IProperty } from 'src/app/common/interface/property.interface';

@Component({
  selector: 'app-property-grid',
  templateUrl: './property-grid.component.html',
  styleUrls: ['./property-grid.component.scss']
})
export class PropertyGridComponent implements OnInit {
  @Input() schematicId!: string;

  gridConfig!: IGridConfig<IProperty>;

  private _selected: IProperty[] = [];

  constructor(
    private readonly injector: Injector
  ) { }

  ngOnInit(): void {
    this.gridConfig = new PropertyGridConfig(this.injector, this.schematicId);
  }

  get selected(): IProperty[] {
    return this._selected;
  }

  onSelectedChange(event: IProperty[]): void {
    this._selected = event;
  }
}
