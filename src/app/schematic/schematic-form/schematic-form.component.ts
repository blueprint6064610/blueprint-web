import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ISchematic } from 'src/app/common/interface/schematic.interface';

@Component({
  selector: 'app-schematic-form',
  templateUrl: './schematic-form.component.html',
  styleUrls: ['./schematic-form.component.scss']
})
export class SchematicFormComponent implements OnInit {
  @Input() schematic?: ISchematic;

  schematicForm = this.formBuilder.group({
    name: ['', Validators.required],
    description: ['']
  });

  constructor(
    private readonly formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    if (this.schematic) {  
      const formData = {
        name: this.schematic.name,
        description: this.schematic.description,
      };

      this.schematicForm.setValue(formData);
    }
  }
}
