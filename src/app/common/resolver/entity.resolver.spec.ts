import { TestBed } from '@angular/core/testing';

import { EntityResolver } from './entity.resolver';

describe('EntityResolver', () => {
  let resolver: EntityResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(EntityResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
