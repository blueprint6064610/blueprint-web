/**
 * A custom entity object.
 */
export interface ICustomEntity {
  /**
   * The id of the object.
   */
  id: string;

  /**
   * The custom properties of the object.
   */
  [key: string]: any;
}
