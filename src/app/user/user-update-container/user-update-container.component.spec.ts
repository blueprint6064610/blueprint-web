import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserUpdateContainerComponent } from './user-update-container.component';

describe('UserUpdateContainerComponent', () => {
  let component: UserUpdateContainerComponent;
  let fixture: ComponentFixture<UserUpdateContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserUpdateContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserUpdateContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
