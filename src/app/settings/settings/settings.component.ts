import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { INavigation } from 'src/app/common/interface/navigation.interface';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  navigations: INavigation[] = [
    {
      icon: 'table_view',
      label: 'Schematics',
      route: 'schematics'
    },
    {
      icon: 'manage_accounts',
      label: 'Users',
      route: 'users'
    }
  ];

  constructor(
    private readonly router: Router
  ) { }

  async ngOnInit(): Promise<void> {
    if (this.router.url === '/settings') {
      const first = this.navigations[0];
      await this.router.navigate(['settings', first.route]);
    }
  }
}
