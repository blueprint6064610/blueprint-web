import { IFilter } from "./filter.interface";
import { IPagination } from "./pagination.interface";
import { ISort } from "./sort.interface";

/**
 * A request interface for any queryable set.
 */
export interface IQueryRequest {
  /**
   * The pagination parameters.
   */
  pagination?: IPagination;

  /**
   * The sorting parameters.
   */
  sort?: ISort[];

  /**
   * The filtering parameters.
   */
  filter?: IFilter;
}
