import { Component } from '@angular/core';
import { ControlBaseComponent } from '../control-base/control-base.component';

@Component({
  selector: 'app-control-integer',
  templateUrl: './control-integer.component.html',
  styleUrls: ['./control-integer.component.scss']
})
export class ControlIntegerComponent extends ControlBaseComponent { }
