import { ConfirmDialogTheme } from "./confirm-dialog-theme.interface";

export interface IConfirmDialogData {
  title: string;
  message: string;
  action: string;
  theme: ConfirmDialogTheme;
}
