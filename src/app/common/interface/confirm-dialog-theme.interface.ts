/**
 * The confirm dialog theme.
 */
export type ConfirmDialogTheme = 'info' | 'warn';
