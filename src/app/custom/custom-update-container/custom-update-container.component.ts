import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ICustomEntity } from 'src/app/common/interface/custom-entity.interface';
import { ISchematic } from 'src/app/common/interface/schematic.interface';
import { CustomService } from 'src/app/common/service/custom.service';
import { CustomFormComponent } from '../custom-form/custom-form.component';

@Component({
  selector: 'app-custom-update-container',
  templateUrl: './custom-update-container.component.html',
  styleUrls: ['./custom-update-container.component.scss']
})
export class CustomUpdateContainerComponent implements OnInit {
  @ViewChild(CustomFormComponent) customForm!: CustomFormComponent;

  schematic!: ISchematic;

  entity!: ICustomEntity;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly customService: CustomService,
    private readonly router: Router
  ) { }

  get form(): FormArray {
    return this.customForm.customForm;
  }

  ngOnInit(): void {
    this.schematic = this.activatedRoute.snapshot.data['schematic'];
    this.entity = this.activatedRoute.snapshot.data['entity'];
  }

  update(): void {
    const schematicId = this.schematic.id;
    const entityId = this.entity.id;
    const request = this.schematic.properties.reduce((prev, curr, index) => {
      prev[curr.id] = this.form.value[index];
      return prev;
    }, {} as Record<string, any>);

    this.customService.update(schematicId, entityId, request).subscribe(async () => {
      await this.router.navigate([schematicId]);
    });
  }
}
