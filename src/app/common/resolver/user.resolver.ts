import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { IUser } from '../interface/user.interface';
import { UserService } from '../service/user.service';

/**
 * Resolves a user.
 */
@Injectable({
  providedIn: 'root'
})
export class UserResolver implements Resolve<IUser> {
  /**
   * UserResolver constructor.
   */
  constructor(
    private readonly userService: UserService
  ) { }

  /**
   * @inheritdoc
   */
  resolve(route: ActivatedRouteSnapshot): Observable<IUser> {
    const id = route.paramMap.get('userId');
    if (!id) { throw new Error(':userId param not found'); }
    return this.userService.findById(id);
  }
}
