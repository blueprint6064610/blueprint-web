import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ICustomEntity } from 'src/app/common/interface/custom-entity.interface';
import { IProperty } from 'src/app/common/interface/property.interface';
import { ISchematic } from 'src/app/common/interface/schematic.interface';

@Component({
  selector: 'app-custom-form',
  templateUrl: './custom-form.component.html',
  styleUrls: ['./custom-form.component.scss']
})
export class CustomFormComponent implements OnInit {
  customForm = this.formBuilder.array([]);

  @Input() schematic!: ISchematic;

  @Input() entity?: ICustomEntity;

  constructor(
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.schematic.properties.forEach(() => {
      this.customForm.push(new FormControl('', Validators.required));
    });

    if (this.entity) {
      this.schematic.properties.forEach((property, index) => {
        const value = this.entity![property.id];
        this.customForm.controls[index].setValue(value);
      });
    }
  }

  getProperty(index: number): IProperty {
    return this.schematic.properties[index];
  }
}
