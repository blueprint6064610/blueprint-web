/**
 * A navigation item.
 */
export interface INavigation {
  /**
   * The navigation icon.
   */
  icon: string;

  /**
   * The navigation label.
   */
  label: string;

  /**
   * The navigation URL route.
   */
  route: string;
}
