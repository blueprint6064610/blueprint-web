import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SchematicResolver } from "../common/resolver/schematic.resolver";
import { SchematicCreateContainerComponent } from "./schematic-create-container/schematic-create-container.component";
import { SchematicGridContainerComponent } from "./schematic-grid-container/schematic-grid-container.component";
import { SchematicUpdateContainerComponent } from "./schematic-update-container/schematic-update-container.component";

const routes: Routes = [
  {
    path: 'create',
    component: SchematicCreateContainerComponent
  },
  {
    path: ':schematicId',
    component: SchematicUpdateContainerComponent,
    resolve: { schematic: SchematicResolver }
  },
  {
    path: '',
    component: SchematicGridContainerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SchematicRoutingModule { }
