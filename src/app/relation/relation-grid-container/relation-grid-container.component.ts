import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ISchematic } from 'src/app/common/interface/schematic.interface';
import { RelationService } from 'src/app/common/service/relation.service';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
import { RelationCreateDialogComponent } from '../relation-create-dialog/relation-create-dialog.component';
import { RelationGridComponent } from '../relation-grid/relation-grid.component';
import { RelationUpdateDialogComponent } from '../relation-update-dialog/relation-update-dialog.component';

@Component({
  selector: 'app-relation-grid-container',
  templateUrl: './relation-grid-container.component.html',
  styleUrls: ['./relation-grid-container.component.scss']
})
export class RelationGridContainerComponent implements OnInit {
  @ViewChild('grid') grid?: RelationGridComponent;

  schematic!: ISchematic;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly matDialog: MatDialog,
    private readonly relationService: RelationService
  ) { }

  get isOneSelected(): boolean {
    return this.grid?.selected.length === 1;
  }

  ngOnInit(): void {
    this.schematic = this.activatedRoute.snapshot.data['schematic'];
  }

  create(): void {
    this.matDialog.open(RelationCreateDialogComponent, {
      data: { schematicId: this.schematic.id },
      width: '50vw',
      disableClose: true
    }).afterClosed().subscribe((refresh: boolean) => {
      if (refresh) window.location.reload();
    });
  }

  update(): void {
    const relation = this.grid?.selected[0];
    if (!relation) return;

    this.matDialog.open(RelationUpdateDialogComponent, {
      data: { relation, schematicId: this.schematic.id },
      width: '50vw',
      disableClose: true
    }).afterClosed().subscribe((refresh: boolean) => {
      if (refresh) window.location.reload();
    });
  }

  delete(): void {
    const relation = this.grid?.selected[0];
    if (!relation) return;

    this.matDialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Delete Relation',
        message: 'Are you sure you want to delete this property?',
        action: 'Delete',
        theme: 'warn'
      }
    }).afterClosed().subscribe((confirm: boolean) => {
      if (!confirm) return;
      this.relationService.delete(relation.id).subscribe(() => {
        window.location.reload();
      });
    });
  }
}
