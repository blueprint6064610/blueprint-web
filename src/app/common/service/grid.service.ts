import { Injectable, Injector } from '@angular/core';
import { PropertyTypeGridConfig } from '../grid-config/property-type.grid-config';
import { SchematicGridConfig } from '../grid-config/schematic.grid-config';
import { IGridConfig } from '../interface/grid-config.interface';

type GridConfigConstructor<T> = new (injector: Injector) => IGridConfig<T>;

@Injectable({
  providedIn: 'root'
})
export class GridService {

  private readonly configurations = new Map<string, GridConfigConstructor<any>>();

  constructor(
    private readonly injector: Injector
  ) {
    this.setDefaultConfigs();
  }

  get<T>(key: string): IGridConfig<T> {
    if (!this.configurations.has(key)) {
      throw new Error(`No grid configuration for ${key}`);
    }

    const GridConfig = this.configurations.get(key)!;
    return new GridConfig(this.injector);
  }

  set<T>(key: string, config: GridConfigConstructor<T>): void {
    if (!this.configurations.has(key)) {
      this.configurations.set(key, config);
    }
  }

  private setDefaultConfigs(): void {
    this.configurations.set('property-type', PropertyTypeGridConfig);
    this.configurations.set('schematic', SchematicGridConfig);
  }
}
