import { RelationType } from "./relation-type.interface";
import { ISchematic } from "./schematic.interface";

/**
 * A relation between two schematics.
 */
export interface IRelation {
  /**
   * The relation id.
   */
  id: string;

  /**
   * The relation name.
   */
  name: string;

  /**
   * The relation description.
   */
  description: string;

  /**
   * The schematic on the left side of the relation.
   */
  lhsSchematic: ISchematic;

  /**
   * The schematic on the right side of the relation.
   */
  rhsSchematic: ISchematic;

  /**
   * The relation type between the two schematics.
   */
  relationType: RelationType;
}
