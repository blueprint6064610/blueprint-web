import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ISchematic } from 'src/app/common/interface/schematic.interface';
import { CustomService } from 'src/app/common/service/custom.service';
import { SchematicGridComponent } from 'src/app/schematic/schematic-grid/schematic-grid.component';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-custom-grid-container',
  templateUrl: './custom-grid-container.component.html',
  styleUrls: ['./custom-grid-container.component.scss']
})
export class CustomGridContainerComponent implements OnInit {
  @ViewChild('grid') grid?: SchematicGridComponent;

  schematic!: ISchematic;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly customService: CustomService,
    private readonly matDialog: MatDialog,
    private readonly router: Router
  ) { }

  get isOneSelected(): boolean {
    return this.grid?.selected.length === 1;
  }

  ngOnInit(): void {
    this.schematic = this.activatedRoute.snapshot.data['schematic'];
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  async create(): Promise<void> {
    const id = this.schematic.id;
    await this.router.navigate([id, 'create']);
  }

  async update(): Promise<void> {
    const schematicId: string = this.schematic.id;
    const entityId: string | undefined = this.grid?.selected[0]?.id;
    await this.router.navigate([schematicId, entityId]);
  }

  delete(): void {
    const entity = this.grid?.selected[0];
    if (!entity) { return; }

    this.matDialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Delete Entity',
        message: 'Are you sure you want to delete this entity?',
        action: 'Delete',
        theme: 'warn'
      }
    }).afterClosed().subscribe((confirm: boolean) => {
      if (!confirm) { return; }
      const schematicId = this.schematic.id;
      this.customService.delete(schematicId, entity.id).subscribe(() => {
        window.location.reload();
      });
    });
  }
}
