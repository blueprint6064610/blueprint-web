import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { IProperty } from 'src/app/common/interface/property.interface';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss']
})
export class ControlComponent {
  @Input() control!: FormControl;

  @Input() property!: IProperty;

  get code(): any {
    return this.property.propertyType.code;
  }
}
