/**
 * Specifies sorting parameters.
 */
export interface ISort {
  /**
   * The field key to sort.
   */
  field: string;

  /**
   * The direction to sort.
   */
  direction: 'asc' | 'desc';
}
