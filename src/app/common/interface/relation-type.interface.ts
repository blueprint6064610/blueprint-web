/**
 * The relation type.
 */
export type RelationType = 'OneToOne' | 'OneToMany' | 'ManyToOne' | 'ManyToMany';
