import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ICustomEntity } from '../interface/custom-entity.interface';
import { CustomService } from '../service/custom.service';

/**
 * Resolves an entity.
 */
@Injectable({
  providedIn: 'root'
})
export class EntityResolver implements Resolve<ICustomEntity> {
  /**
   * EntityResolver constructor.
   */
  constructor(
    private readonly customService: CustomService
  ) { }

  /**
   * @inheritdoc
   */
  resolve(route: ActivatedRouteSnapshot): Observable<ICustomEntity> {
    const schematicId = route.paramMap.get('schematicId');
    const entityId = route.paramMap.get('entityId');
    if (!schematicId || !entityId) { throw new Error(':schematicId or :entityId not found'); }
    return this.customService.findById(schematicId, entityId);
  }
}
