import { Component, Injector, OnInit } from '@angular/core';
import { UserGridConfig } from 'src/app/common/grid-config/user.grid-config';
import { IUser } from 'src/app/common/interface/user.interface';

@Component({
  selector: 'app-user-grid',
  templateUrl: './user-grid.component.html',
  styleUrls: ['./user-grid.component.scss']
})
export class UserGridComponent implements OnInit {
  gridConfig!: UserGridConfig;

  private _selected: IUser[] = [];

  constructor(
    private readonly injector: Injector
  ) { }

  get selected(): IUser[] {
    return this._selected;
  }

  ngOnInit(): void {
    this.gridConfig = new UserGridConfig(this.injector);
  }

  onSelectedChange(event: IUser[]): void {
    this._selected = event;
  }
}
