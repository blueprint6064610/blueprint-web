/**
 * The item selector configuration.
 */
export interface IItemSelectorConfig<T> {
  /**
   * Gets the display value of the selected value.
   * @param value The value.
   */
  getLabel(value: T): string;
}
