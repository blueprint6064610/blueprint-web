import { SelectionModel } from '@angular/cdk/collections';
import { Component, Input, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { IGridColumn } from 'src/app/common/interface/grid-column.interface';
import { IGridConfig } from 'src/app/common/interface/grid-config.interface';
import { IQueryRequest } from 'src/app/common/interface/query-request.interface';
import { ServerDataSource } from '../../common/data-source/server.data-source';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent<T> implements OnInit {
  @ViewChild('paginator') paginator?: MatPaginator;

  @Input() gridConfig!: IGridConfig<T>;

  @Output() selectedChange = new EventEmitter<T[]>();

  filter: Record<string, string> = {};

  private selection = new SelectionModel<T>(false);

  get columns(): IGridColumn[] {
    return this.gridConfig.columns;
  }

  get dataSource(): ServerDataSource<T> {
    return this.gridConfig.dataSource;
  }

  get getId(): (row: T) => any {
    return this.gridConfig.getId;
  }

  get select(): boolean {
    return this.gridConfig.select;
  }

  get displayedColumns(): string[] {
    const keys = this.columns.map(column => column.field);
    if (this.select) { keys.splice(0, 0, 'select'); }
    return keys;
  }

  get filterColumns(): string[] {
    const keys = this.columns.map(this.getFilterKey);
    if (this.select) { keys.splice(0, 0, 'blank'); }
    return keys;
  }

  get pageSizeOptions(): number[] {
    return [10, 25, 50, 100];
  }

  ngOnInit(): void {
    this.buildFilter();
    this.refresh();
  }

  buildFilter(): void {
    this.columns.forEach(col => {
      this.filter[col.field] = '';
    });
  }

  deselectAll(): void {
    this.selection.clear();
    this.updateSelectedChange();
  }

  getFilterKey(column: IGridColumn): string {
    return `${column.field}-filter`;
  }

  isSelected(row: T): boolean {
    const target = this.getId(row);
    return this.selection.selected.some(selected => this.getId(selected) === target);
  }

  selectedToggle(row: T): void {
    const target = this.getId(row);
    const exists = this.selection.selected.find(selected => this.getId(selected) === target);

    if (exists) {
      this.selection.toggle(exists);
    } else {
      this.selection.toggle(row);
    }

    this.updateSelectedChange();
  }

  onFilterChange(): void {
    this.refresh();
  }

  refresh(): void {
    const query: IQueryRequest = {
      pagination: {
        page: this.paginator?.pageIndex ?? 0,
        pageSize: this.paginator?.pageSize ?? 10
      },
      filter: this.filter
    };

    this.dataSource.fetch(query);
  }

  private updateSelectedChange(): void {
    const values = this.selection.selected;
    this.selectedChange.emit(values);
  }
}
