import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IPropertyType } from '../interface/property-type.interface';
import { IQueryRequest } from '../interface/query-request.interface';
import { IQueryResponse } from '../interface/query-response.interface';
import { QueryService } from './query.service';

@Injectable({
  providedIn: 'root'
})
export class PropertyTypeService {
  private static readonly API_URL = `${environment.apiBase}/property-types`;

  constructor(
    private readonly httpClient: HttpClient,
    private readonly queryService: QueryService
  ) { }

  find(query: IQueryRequest): Observable<IQueryResponse<IPropertyType>> {
    const url = PropertyTypeService.API_URL;
    const params = this.queryService.getParams(query);
    return this.httpClient.get<IQueryResponse<IPropertyType>>(url, { params });
  }

  findById(id: string): Observable<IPropertyType> {
    const url = `${PropertyTypeService.API_URL}/${id}`;
    return this.httpClient.get<IPropertyType>(url);
  }
}
