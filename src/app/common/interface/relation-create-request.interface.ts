import { RelationType } from "./relation-type.interface";

/**
 * Create a relation.
 */
export interface IRelationCreateRequest {
  /**
   * The relation name.
   */
  name: string;

  /**
   * The relation description.
   */
  description: string;

  /**
   * The schematic id of the left side of the relation.
   */
  lhsSchematicId: string;

  /**
   * The schematic id of the right side of the relation.
   */
  rhsSchematicId: string;

  /**
   * The relation type between the two schematics.
   */
  relationType: RelationType
}
