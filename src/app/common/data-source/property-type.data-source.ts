import { Observable } from "rxjs";
import { IPropertyType } from "../interface/property-type.interface";
import { IQueryRequest } from "../interface/query-request.interface";
import { IQueryResponse } from "../interface/query-response.interface";
import { PropertyTypeService } from "../service/property-type.service";
import { ServerDataSource } from "./server.data-source";

/**
 * A data source for querying property types.
 */
export class PropertyTypeDataSource extends ServerDataSource<IPropertyType> {
  /**
   * PropertyTypeDataSource constructor.
   * @param propertyTypeService The property type service.
   */
  constructor(
    private readonly propertyTypeService: PropertyTypeService
  ) {
    super();
  }

  /**
   * @inheritdoc
   */
  protected override dataSource$(query: IQueryRequest): Observable<IQueryResponse<IPropertyType>> {
    return this.propertyTypeService.find(query);
  }
}
