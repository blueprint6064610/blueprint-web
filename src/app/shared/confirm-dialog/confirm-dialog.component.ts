import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IConfirmDialogData } from 'src/app/common/interface/confirm-dialog-data.interface';

/**
 * Confirm dialog component.
 */
@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {
  /**
   * ConfirmDialogComponent constructor.
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) private readonly data: IConfirmDialogData
  ) { }

  /**
   * Get the title.
   */
  get title(): string {
    return this.data.title;
  }

  /**
   * Get the message.
   */
  get message(): string {
    return this.data.message;
  }

  /**
   * Get the action.
   */
  get action(): string {
    return this.data.action;
  }

  /**
   * Get the colour based on the theme.
   */
  get color(): string {
    switch (this.data.theme) {
      case 'info': return '#2196f3';
      case 'warn': return '#f44336';
      default: return '#2196f3';
    }
  }
}
