/**
 * A create property request.
 */
export interface IPropertyCreateRequest {
  /**
   * The property name.
   */
  name: string;

  /**
   * The property description.
   */
  description?: string;

  /**
   * The property type id.
   */
  propertyTypeId: string;
}
