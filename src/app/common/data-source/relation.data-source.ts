import { Observable } from "rxjs";
import { ServerDataSource } from "./server.data-source";
import { RelationService } from "../service/relation.service";
import { IRelation } from "../interface/relation.interface";
import { IQueryRequest } from "../interface/query-request.interface";
import { IQueryResponse } from "../interface/query-response.interface";

/**
 * A data source for querying relations.
 */
export class RelationDataSource extends ServerDataSource<IRelation> {
  /**
   * RelationDataSource constructor.
   * @param relationService The relation service.
   * @param schematicId The schematic id.
   */
  constructor(
    private readonly relationService: RelationService,
    private readonly schematicId: string
  ) {
    super();
  }

  /**
   * @inheritdoc
   */
  protected override dataSource$(query: IQueryRequest): Observable<IQueryResponse<IRelation>> {
    return this.relationService.findBySchematic(this.schematicId, query);
  }
}
