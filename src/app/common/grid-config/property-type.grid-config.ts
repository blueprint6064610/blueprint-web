import { Injector } from "@angular/core";
import { PropertyTypeDataSource } from "../data-source/property-type.data-source";
import { IGridConfig } from "../interface/grid-config.interface";
import { IPropertyType } from "../interface/property-type.interface";
import { PropertyTypeService } from "../service/property-type.service";

/**
 * Configuration class for a property type grid.
 */
export class PropertyTypeGridConfig implements IGridConfig<IPropertyType> {
  /**
   * @inheritdoc
   */
  columns = [
    { field: 'name', headerText: 'Name' },
    { field: 'description', headerText: 'Description' }
  ];

  /**
   * @inheritdoc
   */
  dataSource = new PropertyTypeDataSource(this.injector.get(PropertyTypeService));

  /**
   * @inheritdoc
   */
  select = true;

  /**
   * PropertyTypeGridConfig constructor.
   * @param injector The injector.
   */
  constructor(
    private readonly injector: Injector
  ) { }

  /**
   * @inheritdoc
   */
  getId(row: IPropertyType): any {
    return row.id;
  }
}
