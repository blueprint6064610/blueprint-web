import { Injector } from "@angular/core";
import { CustomDataSource } from "../data-source/custom.data-source";
import { ICustomEntity } from "../interface/custom-entity.interface";
import { IGridConfig } from "../interface/grid-config.interface";
import { ISchematic } from "../interface/schematic.interface";
import { CustomService } from "../service/custom.service";

/**
 * Configuration class for a custom grid.
 */
export class CustomGridConfig implements IGridConfig<ICustomEntity> {
  /**
   * @inheritdoc
   */
  columns = this.schematic.properties.map(property => ({
    field: property.id,
    headerText: property.name
  }));

  /**
   * @inheritdoc
   */
  dataSource = new CustomDataSource(this.injector.get(CustomService), this.schematic.id);
  
  /**
   * @inheritdoc
   */
  select = true;
  
  /**
   * CustomGridConfig constructor.
   * @param injector The injector.
   * @param schematic The schematic.
   */
  constructor(
    private readonly injector: Injector,
    private readonly schematic: ISchematic
  ) { }
  
  /**
   * @inheritdoc
   */
  getId(row: ICustomEntity): any {
    return row.id
  }
}
