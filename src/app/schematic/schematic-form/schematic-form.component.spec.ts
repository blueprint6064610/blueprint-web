import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SchematicFormComponent } from './schematic-form.component';

describe('SchematicFormComponent', () => {
  let component: SchematicFormComponent;
  let fixture: ComponentFixture<SchematicFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchematicFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchematicFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
