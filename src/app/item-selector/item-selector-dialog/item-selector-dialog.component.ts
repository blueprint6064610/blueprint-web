import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IGridConfig } from 'src/app/common/interface/grid-config.interface';
import { IItemSelectorDialogData } from 'src/app/common/interface/item-selector-dialog-data.interface';
import { GridService } from 'src/app/common/service/grid.service';
import { GridComponent } from 'src/app/grid/grid/grid.component';

/**
 * Class ItemSelectorDialogComponent.
 */
@Component({
  selector: 'app-item-selector-dialog',
  templateUrl: './item-selector-dialog.component.html',
  styleUrls: ['./item-selector-dialog.component.scss']
})
export class ItemSelectorDialogComponent implements OnInit {
  @ViewChild('grid') grid!: GridComponent<any>;

  private _gridConfig!: IGridConfig<any>;

  private _selected: any[] = [];

  /**
   * ItemSelectorDialogComponent constructor.
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) private readonly data: IItemSelectorDialogData,
    private readonly gridService: GridService,
    private readonly matDialogRef: MatDialogRef<ItemSelectorDialogComponent>,
  ) { }

  /**
   * If any items have been selected.
   */
  get isAnySelected(): boolean {
    return this._selected.length > 0;
  }

  /**
   * Get the grid options.
   */
  get gridConfig(): IGridConfig<any> {
    return this._gridConfig;
  }

  /**
   * @inheritdoc
   */
  ngOnInit(): void {
    this._gridConfig = this.gridService.get(this.data.key);
  }

  /**
   * Clears the current selection.
   */
  clear(): void {
    this.grid.deselectAll();
  }

  /**
   * Submits the current selection.
   */
  confirm(): void {
    const value = this._selected[0] ?? null;
    this.matDialogRef.close(value);
  }

  onSelectedChange(selected: any[]): void {
    this._selected = selected;
  }
}
