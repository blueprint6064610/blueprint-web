/**
 * The login response.
 */
export interface ILoginResponse {
  /**
   * The JWT encoded in base64.
   */
  token: string;
}
