import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchematicCreateContainerComponent } from './schematic-create-container.component';

describe('SchematicCreateContainerComponent', () => {
  let component: SchematicCreateContainerComponent;
  let fixture: ComponentFixture<SchematicCreateContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchematicCreateContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchematicCreateContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
