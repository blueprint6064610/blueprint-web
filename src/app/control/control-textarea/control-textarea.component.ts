import { Component } from '@angular/core';
import { ControlBaseComponent } from '../control-base/control-base.component';

@Component({
  selector: 'app-control-textarea',
  templateUrl: './control-textarea.component.html',
  styleUrls: ['./control-textarea.component.scss']
})
export class ControlTextareaComponent extends ControlBaseComponent { }
