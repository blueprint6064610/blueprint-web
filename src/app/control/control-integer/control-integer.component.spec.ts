import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlIntegerComponent } from './control-integer.component';

describe('ControlIntegerComponent', () => {
  let component: ControlIntegerComponent;
  let fixture: ComponentFixture<ControlIntegerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlIntegerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlIntegerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
