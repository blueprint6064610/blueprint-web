import { TestBed } from '@angular/core/testing';

import { SchematicResolver } from './schematic.resolver';

describe('SchematicResolver', () => {
  let resolver: SchematicResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(SchematicResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
