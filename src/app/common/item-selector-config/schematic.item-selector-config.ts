import { IItemSelectorConfig } from "../interface/item-selector-config.interface";
import { ISchematic } from "../interface/schematic.interface";

export class SchematicItemSelectorConfig implements IItemSelectorConfig<ISchematic> {
  /**
   * @inheritdoc
   */
  getLabel(value: ISchematic): string {
    return value.name;
  }
}