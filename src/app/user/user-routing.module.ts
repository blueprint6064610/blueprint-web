import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserResolver } from '../common/resolver/user.resolver';
import { UserCreateContainerComponent } from './user-create-container/user-create-container.component';
import { UserGridContainerComponent } from './user-grid-container/user-grid-container.component';
import { UserUpdateContainerComponent } from './user-update-container/user-update-container.component';

const routes: Routes = [
  {
    path: 'create',
    component: UserCreateContainerComponent
  },
  {
    path: ':userId',
    component: UserUpdateContainerComponent,
    resolve: { user: UserResolver }
  },
  {
    path: '',
    component: UserGridContainerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
