import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ControlComponent } from './control/control.component';
import { ControlTextareaComponent } from './control-textarea/control-textarea.component';
import { ControlDecimalComponent } from './control-decimal/control-decimal.component';
import { ControlIntegerComponent } from './control-integer/control-integer.component';
import { ControlTextComponent } from './control-text/control-text.component';
import { ControlBaseComponent } from './control-base/control-base.component';

@NgModule({
  declarations: [
    ControlComponent,
    ControlTextareaComponent,
    ControlDecimalComponent,
    ControlIntegerComponent,
    ControlTextComponent,
    ControlBaseComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  exports: [
    ControlComponent
  ]
})
export class ControlModule { }
