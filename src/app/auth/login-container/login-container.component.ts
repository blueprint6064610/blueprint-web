import { Component } from '@angular/core';

/**
 * A container component for the login form.
 */
@Component({
  selector: 'app-login-container',
  templateUrl: './login-container.component.html',
  styleUrls: ['./login-container.component.scss']
})
export class LoginContainerComponent {
}
