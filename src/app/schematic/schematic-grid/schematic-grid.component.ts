import { Component, Injector, OnInit } from '@angular/core';
import { SchematicGridConfig } from 'src/app/common/grid-config/schematic.grid-config';
import { IGridConfig } from 'src/app/common/interface/grid-config.interface';
import { ISchematic } from 'src/app/common/interface/schematic.interface';

@Component({
  selector: 'app-schematic-grid',
  templateUrl: './schematic-grid.component.html',
  styleUrls: ['./schematic-grid.component.scss']
})
export class SchematicGridComponent implements OnInit {
  gridConfig!: IGridConfig<ISchematic>;

  private _selected: ISchematic[] = [];

  constructor(
    private readonly injector: Injector
  ) { }

  get selected(): ISchematic[] {
    return this._selected;
  }

  ngOnInit(): void {
    this.gridConfig = new SchematicGridConfig(this.injector);
  }

  onSelectedChange(event: ISchematic[]): void {
    this._selected = event;
  }
}
