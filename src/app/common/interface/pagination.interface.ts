/**
 * Specifies paging parameters.
 */
export interface IPagination {
  /**
   * The page index.
   */
  page: number;

  /**
   * The number of items in the page.
   */
  pageSize: number;
}
