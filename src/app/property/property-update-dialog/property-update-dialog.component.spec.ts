import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyUpdateDialogComponent } from './property-update-dialog.component';

describe('PropertyUpdateDialogComponent', () => {
  let component: PropertyUpdateDialogComponent;
  let fixture: ComponentFixture<PropertyUpdateDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PropertyUpdateDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyUpdateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
