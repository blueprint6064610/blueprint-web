import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyCreateDialogComponent } from './property-create-dialog.component';

describe('PropertyCreateDialogComponent', () => {
  let component: PropertyCreateDialogComponent;
  let fixture: ComponentFixture<PropertyCreateDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PropertyCreateDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
