import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyGridContainerComponent } from './property-grid-container.component';

describe('PropertyGridContainerComponent', () => {
  let component: PropertyGridContainerComponent;
  let fixture: ComponentFixture<PropertyGridContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PropertyGridContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyGridContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
