import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { QueryService } from './query.service';
import { ISchematicCreateRequest } from '../interface/schematic-create-request.interface';
import { ISchematic } from '../interface/schematic.interface';
import { ISchematicUpdateRequest } from '../interface/schematic-update-request.interface';
import { IQueryRequest } from '../interface/query-request.interface';
import { IQueryResponse } from '../interface/query-response.interface';

@Injectable({
  providedIn: 'root'
})
export class SchematicService {
  private static readonly API_URL = `${environment.apiBase}/schematics`;

  constructor(
    private readonly httpClient: HttpClient,
    private readonly queryService: QueryService
  ) { }

  create(request: ISchematicCreateRequest): Observable<void> {
    const url = SchematicService.API_URL;
    return this.httpClient.post<void>(url, request);
  }

  delete(id: string): Observable<void> {
    const url = `${SchematicService.API_URL}/${id}`;
    return this.httpClient.delete<void>(url);
  }

  find(query: IQueryRequest): Observable<IQueryResponse<ISchematic>> {
    const url = SchematicService.API_URL;
    const params = this.queryService.getParams(query);
    return this.httpClient.get<IQueryResponse<ISchematic>>(url, { params });
  }

  findById(id: string): Observable<ISchematic> {
    const url = `${SchematicService.API_URL}/${id}`;
    return this.httpClient.get<ISchematic>(url);
  }

  update(id: string, request: ISchematicUpdateRequest): Observable<void> {
    const url = `${SchematicService.API_URL}/${id}`;
    return this.httpClient.put<void>(url, request);
  }
}
