import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { EntityResolver } from "../common/resolver/entity.resolver";
import { SchematicResolver } from "../common/resolver/schematic.resolver";
import { CustomCreateContainerComponent } from "./custom-create-container/custom-create-container.component";
import { CustomGridContainerComponent } from "./custom-grid-container/custom-grid-container.component";
import { CustomUpdateContainerComponent } from "./custom-update-container/custom-update-container.component";

const routes: Routes = [
  {
    path: 'create',
    component: CustomCreateContainerComponent,
    resolve: { schematic: SchematicResolver }
  },
  {
    path: ':entityId',
    component: CustomUpdateContainerComponent,
    resolve: { entity: EntityResolver }
  },
  {
    path: '',
    component: CustomGridContainerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomRoutingModule { }
