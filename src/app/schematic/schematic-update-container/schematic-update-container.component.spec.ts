import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchematicUpdateContainerComponent } from './schematic-update-container.component';

describe('SchematicUpdateContainerComponent', () => {
  let component: SchematicUpdateContainerComponent;
  let fixture: ComponentFixture<SchematicUpdateContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchematicUpdateContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchematicUpdateContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
