import { Component, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ISchematicCreateRequest } from 'src/app/common/interface/schematic-create-request.interface';
import { SchematicService } from 'src/app/common/service/schematic.service';
import { SchematicFormComponent } from '../schematic-form/schematic-form.component';

@Component({
  selector: 'app-schematic-create-container',
  templateUrl: './schematic-create-container.component.html',
  styleUrls: ['./schematic-create-container.component.scss']
})
export class SchematicCreateContainerComponent {
  @ViewChild(SchematicFormComponent) schematicForm!: SchematicFormComponent;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly schematicService: SchematicService
  ) { }

  get form(): FormGroup {
    return this.schematicForm.schematicForm;
  }

  create(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    const value: ISchematicCreateRequest = this.form.value;
    this.schematicService.create(value).subscribe(async () => {
      await this.router.navigate(['..'], {relativeTo: this.activatedRoute});
    });
  }
}
