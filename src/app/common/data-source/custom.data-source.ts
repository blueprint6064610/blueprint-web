import { Observable } from "rxjs";
import { ServerDataSource } from "./server.data-source";
import { CustomService } from "../service/custom.service";
import { ICustomEntity } from "../interface/custom-entity.interface";
import { IQueryRequest } from "../interface/query-request.interface";
import { IQueryResponse } from "../interface/query-response.interface";

/**
 * A data source for querying custom entities.
 */
export class CustomDataSource extends ServerDataSource<ICustomEntity> {
  /**
   * CustomDataSource constructor.
   * @param customService The custom entity service.
   * @param schematicId The schematic id.
   */
  constructor(
    private readonly customService: CustomService,
    private readonly schematicId: string
  ) {
    super();
  }

  /**
   * @inheritdoc
   */
  protected override dataSource$(query: IQueryRequest): Observable<IQueryResponse<ICustomEntity>> {
    return this.customService.find(this.schematicId, query);
  }
}
