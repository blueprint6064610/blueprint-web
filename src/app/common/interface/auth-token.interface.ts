/**
 * The authentication token.
 */
export interface IAuthToken {
  /**
   * The UNIX timestamp in seconds when the token expires.
   */
  exp: number;
}
