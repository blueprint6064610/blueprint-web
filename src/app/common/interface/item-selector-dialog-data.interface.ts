/**
 * Item selector dialog data.
 */
export interface IItemSelectorDialogData {
  /**
   * The key to fetch configurations for.
   */
  key: string
}
