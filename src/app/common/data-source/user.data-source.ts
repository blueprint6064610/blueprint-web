import { Observable } from 'rxjs';
import { ServerDataSource } from './server.data-source';
import { UserService } from '../service/user.service';
import { IUser } from '../interface/user.interface';
import { IQueryRequest } from '../interface/query-request.interface';
import { IQueryResponse } from '../interface/query-response.interface';

/**
 * A data source for querying users.
 */
export class UserDataSource extends ServerDataSource<IUser> {
  /**
   * UserDataSource constructor.
   * @param userService The user service.
   */
  constructor(
    private readonly userService: UserService
  ) {
    super();
  }

  /**
   * @inheritdoc
   */
  protected override dataSource$(query: IQueryRequest): Observable<IQueryResponse<IUser>> {
    return this.userService.find(query);
  }
}
