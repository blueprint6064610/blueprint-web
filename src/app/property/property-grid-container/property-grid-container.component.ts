import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ISchematic } from 'src/app/common/interface/schematic.interface';
import { PropertyService } from 'src/app/common/service/property.service';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
import { PropertyCreateDialogComponent } from '../property-create-dialog/property-create-dialog.component';
import { PropertyGridComponent } from '../property-grid/property-grid.component';
import { PropertyUpdateDialogComponent } from '../property-update-dialog/property-update-dialog.component';

@Component({
  selector: 'app-property-grid-container',
  templateUrl: './property-grid-container.component.html',
  styleUrls: ['./property-grid-container.component.scss']
})
export class PropertyGridContainerComponent implements OnInit {
  @ViewChild('grid') grid?: PropertyGridComponent;

  schematic!: ISchematic;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly matDialog: MatDialog,
    private readonly propertyService: PropertyService
  ) { }

  get isOneSelected(): boolean {
    return this.grid?.selected.length === 1;
  }

  ngOnInit(): void {
    this.schematic = this.activatedRoute.snapshot.data['schematic'];
  }

  create(): void {
    this.matDialog.open(PropertyCreateDialogComponent, {
      data: {schematicId: this.schematic.id},
      width: '50vw',
      disableClose: true
    }).afterClosed().subscribe((refresh) => {
      if (refresh) window.location.reload();
    });
  }

  update(): void {
    const property = this.grid?.selected[0];
    if (!property) return;

    this.matDialog.open(PropertyUpdateDialogComponent, {
      data: {property, schematicId: this.schematic.id},
      width: '50vw',
      disableClose: true
    }).afterClosed().subscribe((refresh) => {
      if (refresh) window.location.reload();
    });
  }

  delete(): void {
    const property = this.grid?.selected[0];
    if (!property) return;

    this.matDialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Delete Property',
        message: 'Are you sure you want to delete this property?',
        action: 'Delete',
        theme: 'warn'
      }
    }).afterClosed().subscribe((confirm: boolean) => {
      if (!confirm) return;
      const schematicId = this.schematic.id;
      this.propertyService.delete(schematicId, property.id).subscribe(() => {
        window.location.reload();
      });
    });
  }
}
