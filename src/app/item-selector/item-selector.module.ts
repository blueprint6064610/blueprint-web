import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ItemSelectorComponent } from './item-selector/item-selector.component';
import { ItemSelectorDialogComponent } from './item-selector-dialog/item-selector-dialog.component';
import { GridModule } from '../grid/grid.module';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [
    ItemSelectorComponent,
    ItemSelectorDialogComponent
  ],
  imports: [
    CommonModule,
    GridModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule
  ],
  exports: [
    ItemSelectorComponent
  ]
})
export class ItemSelectorModule { }
