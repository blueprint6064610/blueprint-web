import { Injector } from "@angular/core";
import { SchematicDataSource } from "../data-source/schematic.data-source";
import { IGridConfig } from "../interface/grid-config.interface";
import { ISchematic } from "../interface/schematic.interface";
import { SchematicService } from "../service/schematic.service";

/**
 * Configuration class for a schematic grid.
 */
export class SchematicGridConfig implements IGridConfig<ISchematic> {
  /**
   * @inheritdoc
   */
  columns = [
    { field: 'name', headerText: 'Name' },
    { field: 'description', headerText: 'Description' }
  ];

  /**
   * @inheritdoc
   */
  dataSource = new SchematicDataSource(this.injector.get(SchematicService));

  /**
   * @inheritdoc
   */
  select = true;

  /**
   * SchematicGridConfig constructor.
   */
  constructor(
    private readonly injector: Injector
  ) { }

  /**
   * @inheritdoc
   */
  getId(row: ISchematic) {
    return row.id;
  }
}
