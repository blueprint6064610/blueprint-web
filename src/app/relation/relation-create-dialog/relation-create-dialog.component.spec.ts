import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RelationCreateDialogComponent } from './relation-create-dialog.component';

describe('RelationCreateDialogComponent', () => {
  let component: RelationCreateDialogComponent;
  let fixture: ComponentFixture<RelationCreateDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RelationCreateDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelationCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
