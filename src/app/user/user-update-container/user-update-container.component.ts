import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IUserUpdateRequest } from 'src/app/common/interface/user-update-request.interface';
import { IUser } from 'src/app/common/interface/user.interface';
import { UserService } from 'src/app/common/service/user.service';
import { UserFormComponent } from '../user-form/user-form.component';

@Component({
  selector: 'app-user-update-container',
  templateUrl: './user-update-container.component.html',
  styleUrls: ['./user-update-container.component.scss']
})
export class UserUpdateContainerComponent implements OnInit {
  @ViewChild(UserFormComponent) userForm!: UserFormComponent;

  user!: IUser;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly userService: UserService
  ) { }

  ngOnInit(): void {
    this.user = this.activatedRoute.snapshot.data['user'];
  }

  get form(): FormGroup {
    return this.userForm.userForm;
  }

  update(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    const { firstName, lastName, email, password } = this.form.value;
    const id = this.user.id;
    const request: IUserUpdateRequest = { firstName, lastName, email, password };
    this.userService.update(id, request).subscribe(async () => {
      await this.router.navigate(['..'], {relativeTo: this.activatedRoute});
    });
  }
}
