import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { CustomGridComponent } from './custom-grid/custom-grid.component';
import { CustomGridContainerComponent } from './custom-grid-container/custom-grid-container.component';
import { CustomRoutingModule } from './custom-routing.module';
import { GridModule } from '../grid/grid.module';
import { CustomFormComponent } from './custom-form/custom-form.component';
import { CustomCreateContainerComponent } from './custom-create-container/custom-create-container.component';
import { CustomUpdateContainerComponent } from './custom-update-container/custom-update-container.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { ControlModule } from '../control/control.module';

@NgModule({
  declarations: [
    CustomGridComponent,
    CustomGridContainerComponent,
    CustomFormComponent,
    CustomCreateContainerComponent,
    CustomUpdateContainerComponent
  ],
  imports: [
    CommonModule,
    ControlModule,
    CustomRoutingModule,
    GridModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,
    ReactiveFormsModule
  ]
})
export class CustomModule { }
