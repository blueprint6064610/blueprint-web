import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { UserRoutingModule } from './user-routing.module';
import { UserGridComponent } from './user-grid/user-grid.component';
import { GridModule } from '../grid/grid.module';
import { UserGridContainerComponent } from './user-grid-container/user-grid-container.component';
import { UserCreateContainerComponent } from './user-create-container/user-create-container.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserUpdateContainerComponent } from './user-update-container/user-update-container.component';

@NgModule({
  declarations: [
    UserGridComponent,
    UserGridContainerComponent,
    UserCreateContainerComponent,
    UserFormComponent,
    UserUpdateContainerComponent
  ],
  imports: [
    CommonModule,
    GridModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,
    ReactiveFormsModule,
    UserRoutingModule
  ]
})
export class UserModule { }
