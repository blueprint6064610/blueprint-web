import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IRelation } from 'src/app/common/interface/relation.interface';

@Component({
  selector: 'app-relation-form',
  templateUrl: './relation-form.component.html',
  styleUrls: ['./relation-form.component.scss']
})
export class RelationFormComponent implements OnInit {
  @Input() relation!: IRelation;

  relationForm = this.formBuilder.group({
    name: ['', Validators.required],
    description: [''],
    schematic: [null, Validators.required],
    relationType: ['', Validators.required]
  });
  
  constructor(
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    if (this.relation) {
      this.relationForm.patchValue({
        name: this.relation.name,
        description: this.relation.description,
        schematic: this.relation.rhsSchematic,
        relationType: this.relation.relationType
      });

      this.relationForm.get('schematic')?.disable();
      this.relationForm.get('relationType')?.disable();
    }
  }
}
