import { Observable } from "rxjs";
import { ServerDataSource } from "./server.data-source";
import { SchematicService } from "../service/schematic.service";
import { ISchematic } from "../interface/schematic.interface";
import { IQueryRequest } from "../interface/query-request.interface";
import { IQueryResponse } from "../interface/query-response.interface";

/**
 * A data source for querying schematics.
 */
export class SchematicDataSource extends ServerDataSource<ISchematic> {
  /**
   * SchematicDataSource constructor.
   * @param schematicService The schematic service.
   */
  constructor(
    private readonly schematicService: SchematicService
  ) {
    super();
  }

  /**
   * @inheritdoc
   */
  protected override dataSource$(query: IQueryRequest): Observable<IQueryResponse<ISchematic>> {
    return this.schematicService.find(query);
  }
}
