import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlDecimalComponent } from './control-decimal.component';

describe('ControlDecimalComponent', () => {
  let component: ControlDecimalComponent;
  let fixture: ComponentFixture<ControlDecimalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlDecimalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlDecimalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
