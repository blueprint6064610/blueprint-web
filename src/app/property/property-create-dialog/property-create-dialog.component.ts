import { Component, Inject, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IPropertyCreateRequest } from 'src/app/common/interface/property-create-request.interface';
import { IPropertyType } from 'src/app/common/interface/property-type.interface';
import { PropertyService } from 'src/app/common/service/property.service';
import { PropertyFormComponent } from '../property-form/property-form.component';

export interface IPropertyCreateDialogData {
  schematicId: string;
}

@Component({
  selector: 'app-property-create-dialog',
  templateUrl: './property-create-dialog.component.html',
  styleUrls: ['./property-create-dialog.component.scss']
})
export class PropertyCreateDialogComponent {
  @ViewChild(PropertyFormComponent) propertyForm!: PropertyFormComponent;

  constructor(
    @Inject(MAT_DIALOG_DATA) private readonly data: IPropertyCreateDialogData,
    private readonly matDialogRef: MatDialogRef<PropertyCreateDialogComponent>,
    private readonly propertyService: PropertyService
  ) { }

  get form(): FormGroup {
    return this.propertyForm.propertyForm;
  }

  get name(): string {
    return this.form.get('name')?.value;
  }

  get description(): string {
    return this.form.get('description')?.value;
  }

  get propertyType(): IPropertyType {
    return this.form.get('propertyType')?.value;
  }

  create(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    const schematicId = this.data.schematicId;
    const request: IPropertyCreateRequest = {
      name: this.name,
      description: this.description,
      propertyTypeId: this.propertyType.id,
    };

    
    this.propertyService.create(schematicId, request).subscribe(() => {
      this.matDialogRef.close(true);
    });
  }
}
