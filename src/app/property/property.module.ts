import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropertyGridComponent } from './property-grid/property-grid.component';
import { GridModule } from '../grid/grid.module';
import { PropertyGridContainerComponent } from './property-grid-container/property-grid-container.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { PropertyFormComponent } from './property-form/property-form.component';
import { PropertyCreateDialogComponent } from './property-create-dialog/property-create-dialog.component';
import { PropertyUpdateDialogComponent } from './property-update-dialog/property-update-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { ItemSelectorModule } from '../item-selector/item-selector.module';

@NgModule({
  declarations: [
    PropertyGridComponent,
    PropertyGridContainerComponent,
    PropertyFormComponent,
    PropertyCreateDialogComponent,
    PropertyUpdateDialogComponent
  ],
  imports: [
    CommonModule,
    ItemSelectorModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatToolbarModule,
    GridModule,
    ReactiveFormsModule
  ],
  exports: [
    PropertyGridContainerComponent
  ]
})
export class PropertyModule { }
