import { DataSource } from "@angular/cdk/collections";
import { BehaviorSubject, catchError, finalize, Observable, of } from "rxjs";
import { IQueryRequest } from "../interface/query-request.interface";
import { IQueryResponse } from "../interface/query-response.interface";

const EMPTY_RESPONSE: IQueryResponse<any> = {
  total: 0,
  items: []
}

/**
 * A data source used for fetching data from a server.
 */
export abstract class ServerDataSource<T> extends DataSource<T> {
  /**
   * The subject emitting changes in the data.
   */
  private dataSubject = new BehaviorSubject<T[]>([]);

  /**
   * The subject emitting the loading status.
   */
  private loadingSubject = new BehaviorSubject<boolean>(false);

  /**
   * The subject emitting the latest query total.
   */
  private totalSubject = new BehaviorSubject<number>(0);

  /**
   * The observable that gets the data.
   */
  protected abstract dataSource$(query: IQueryRequest): Observable<IQueryResponse<T>>;

  /**
   * Get the loading observable.
   */
  get loading$(): Observable<boolean> {
    return this.loadingSubject;
  }

  /**
   * Get the total observable.
   */
  get total$(): Observable<number> {
    return this.totalSubject;
  }

  /**
   * @inheritdoc
   */
  override connect(): Observable<readonly T[]> {
    return this.dataSubject;
  }

  /**
   * @inheritdoc
   */
  override disconnect(): void {
    this.dataSubject.complete();
    this.loadingSubject.complete();
  }

  fetch(query: IQueryRequest): void {
    this.loadingSubject.next(true);
    this.dataSource$(query).pipe(
      catchError(() => of(EMPTY_RESPONSE)),
      finalize(() => this.loadingSubject.next(false))
    ).subscribe(res => {
      this.dataSubject.next(res.items)
      this.totalSubject.next(res.total);
    });
  }
}
