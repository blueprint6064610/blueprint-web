import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IRelationUpdateRequest } from 'src/app/common/interface/relation-update-request.interface';
import { IRelation } from 'src/app/common/interface/relation.interface';
import { RelationService } from 'src/app/common/service/relation.service';
import { RelationFormComponent } from '../relation-form/relation-form.component';

export interface IRelationUpdateDialogData {
  relation: IRelation;
}

@Component({
  selector: 'app-relation-update-dialog',
  templateUrl: './relation-update-dialog.component.html',
  styleUrls: ['./relation-update-dialog.component.scss']
})
export class RelationUpdateDialogComponent {
  @ViewChild(RelationFormComponent) relationForm!: RelationFormComponent;

  constructor(
    @Inject(MAT_DIALOG_DATA) private readonly data: IRelationUpdateDialogData,
    private readonly matDialogRef: MatDialogRef<RelationUpdateDialogComponent>,
    private readonly relationService: RelationService
  ) { }

  get form(): FormGroup {
    return this.relationForm.relationForm;
  }

  get relation(): IRelation {
    return this.data.relation;
  }

  get name(): string {
    return this.form.get('name')?.value;
  }

  get description(): string {
    return this.form.get('description')?.value;
  }

  update(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    const relationId = this.relation.id;
    const request: IRelationUpdateRequest = {
      name: this.name,
      description: this.description
    };

    this.relationService.update(relationId, request).subscribe(() => {
      this.matDialogRef.close(true);
    });
  }
}
