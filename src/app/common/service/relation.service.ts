import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IQueryRequest } from '../interface/query-request.interface';
import { IQueryResponse } from '../interface/query-response.interface';
import { IRelationCreateRequest } from '../interface/relation-create-request.interface';
import { IRelationUpdateRequest } from '../interface/relation-update-request.interface';
import { IRelation } from '../interface/relation.interface';
import { QueryService } from './query.service';

/**
 * The relation service.
 */
@Injectable({
  providedIn: 'root'
})
export class RelationService {
  /**
   * RelationService constructor.
   */
  constructor(
    private readonly httpClient: HttpClient,
    private readonly queryService: QueryService
  ) { }

  /**
   * Create a relation.
   * @param request The create relation request.
   * @returns The response.
   */
  create(request: IRelationCreateRequest): Observable<void> {
    const url = `${environment.apiBase}/relations`;
    return this.httpClient.post<void>(url, request);
  }

  /**
   * Find relations by schematid id.
   * @param id The schematic id.
   * @param query The query request.
   * @returns The response.
   */
  findBySchematic(id: string, query: IQueryRequest): Observable<IQueryResponse<IRelation>> {
    const url = `${environment.apiBase}/schematics/${id}/relations`;
    const params = this.queryService.getParams(query);
    return this.httpClient.get<IQueryResponse<IRelation>>(url, { params });
  }

  /**
   * Delete a relation.
   * @param id The relation id.
   * @returns The response.
   */
  delete(id: string): Observable<void> {
    const url = `${environment.apiBase}/relations/${id}`;
    return this.httpClient.delete<void>(url);
  }

  /**
   * Update a relation.
   * @param id The relation id.
   * @param request The update relation request.
   * @returns The response.
   */
  update(id: string, request: IRelationUpdateRequest): Observable<void> {
    const url = `${environment.apiBase}/relations/${id}`;
    return this.httpClient.put<void>(url, request);
  }
}
