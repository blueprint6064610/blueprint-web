import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Component, HostBinding, Input, OnDestroy, OnInit, Optional, Self } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldControl } from '@angular/material/form-field';
import { Subject } from 'rxjs';
import { IItemSelectorConfig } from 'src/app/common/interface/item-selector-config.interface';
import { IItemSelectorDialogData } from 'src/app/common/interface/item-selector-dialog-data.interface';
import { ItemSelectorService } from 'src/app/common/service/item-selector.service';
import { ItemSelectorDialogComponent } from '../item-selector-dialog/item-selector-dialog.component';

@Component({
  selector: 'app-item-selector',
  templateUrl: './item-selector.component.html',
  styleUrls: ['./item-selector.component.scss'],
  providers: [{provide: MatFormFieldControl, useExisting: ItemSelectorComponent}]
})
export class ItemSelectorComponent implements ControlValueAccessor, MatFormFieldControl<any>, OnInit, OnDestroy {
  /**
   * The next id to use for the component.
   */
  private static nextId = 0;

  /**
   * @inheritdoc
   */
  @HostBinding() id = `app-item-selector-${ItemSelectorComponent.nextId++}`;

  /**
   * The key to load the configuration for.
   */
  @Input() key!: string;

  /**
   * Get the control type.
   */
  get controlType(): string {
    return 'app-item-selector';
  }

  /**
   * Get if disabled.
   */
  get disabled(): boolean {
    return this._disabled;
  }

  /**
   * Set if disabled.
   */
  @Input() set disabled(disabled: any) {
    this._disabled = coerceBooleanProperty(disabled);
    this.stateChanges.next();
  }

  /**
   * Get described by attributes.
   */
  @HostBinding('attr.aria-describedby') get describedBy(): string[] {
    return this._describedBy;
  }

  /**
   * Set described by attributes.
   */
  set describedBy(describedBy: string[]) {
    this._describedBy = describedBy;
    // No state change required
  }

  /**
   * If the control is empty.
   */
  get empty(): boolean {
    return this.value === null || this.value === undefined;
  }

  /**
   * If the control is in error state.
   */
  get errorState(): boolean {
    return this.ngControl?.errors !== null && !!this.ngControl?.touched;
  }

  /**
   * Get if focused.
   */
  get focused(): boolean {
    return this._focused;
  }

  /**
   * Set if focused.
   */
  set focused(focused: boolean) {
    this._focused = focused;
    this.stateChanges.next();
  }

  /**
   * Get the label.
   */
  get label(): string {
    return this._label;
  }

  /**
   * Set the label.
   */
  set label(label: string) {
    this._label = label;
    this.stateChanges.next();
  }

  /**
   * Get the placeholder.
   */
  get placeholder(): string {
    return this._placeholder;
  }

  /**
   * Set the placeholder.
   */
  set placeholder(placeholder: string) {
    this._placeholder = placeholder;
    this.stateChanges.next();
  }

  /**
   * Get if required.
   */
  get required(): boolean {
    return this._required;
  }

  /**
   * Set if required.
   */
  @Input() set required(required: any) {
    this._required = coerceBooleanProperty(required);
    this.stateChanges.next();
  }

  /**
   * If the label should float.
   */
  get shouldLabelFloat(): boolean {
    return this.focused || !this.empty;
  }

  /**
   * @inheritdoc
   */
  stateChanges = new Subject<void>();

  /**
   * Get the value.
   */
  get value(): any {
    return this._value;
  }

  /**
   * Set the value.
   */
  set value(value: any | null) {
    this._value = value;
    this.stateChanges.next();
  }

  private _itemSelectorConfig!: IItemSelectorConfig<any>;

  /**
   * ARIA described by attributes.
   */
  private _describedBy: string[] = [];

  /**
   * If the control is disabled.
   */
  private _disabled = false;

  /**
   * If the control is focused.
   */
  private _focused = false;

  /**
   * The display label.
   */
  private _label = '';

  /**
   * The on change callback.
   */
  private _onChange?: (value: any) => void;

  /**
   * The on touched callback.
   */
  private _onTouched?: () => void;

  /**
   * The placeholder label.
   */
  private _placeholder = '';

  /**
   * If a value is required.
   */
  private _required = false;

  /**
   * The current value.
   */
  private _value: any = null;

  /**
   * ItemSelectorComponent constructor.
   */
  constructor(
    @Optional() @Self() public ngControl: NgControl,
    private readonly itemSelectorService: ItemSelectorService,
    private readonly matDialog: MatDialog
  ) {
    if (this.ngControl) { this.ngControl.valueAccessor = this; }
  }

  /**
   * @inheritdoc
   */
  ngOnInit(): void {
    this._itemSelectorConfig = this.itemSelectorService.get(this.key);
  }

  /**
   * @inheritdoc
   */
  ngOnDestroy(): void {
    this.stateChanges.complete();
  }

  /**
   * @inheritdoc
   */
  onContainerClick(event: MouseEvent): void {
    if (this.disabled) { return; }

    this.focused = true;

    const data: IItemSelectorDialogData = { key: this.key };
    this.matDialog.open(ItemSelectorDialogComponent, { data, width: '80vw' })
      .afterClosed().subscribe((result: Record<string, any>[] | Record<string, any> | null | undefined) => {
        this.focused = false;
        if (this._onTouched) { this._onTouched(); }

        if (this.setValue(result) && this._onChange) {
          this._onChange(result);
        }
      });
  }

  /**
   * @inheritdoc
   */
  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  /**
   * @inheritdoc
   */
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  /**
   * @inheritdoc
   */
  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }

  /**
   * @inheritdoc
   */
  setDescribedByIds(ids: string[]): void {
    this.describedBy = ids;
  }

  /**
   * @inheritdoc
   */
  writeValue(value: any): void {
    // Workaround for https://github.com/angular/angular/issues/40209
    // writeValue called before ngOnInit
    setTimeout(() => this.setValue(value));
  }

  private setValue(result: Record<string, any>[] | Record<string, any> | null | undefined): boolean {
    if (result === undefined) {
      // Undefined means the user cancelled the dialog
      return false;
    } else if (result === null) {
      // Null means the user chose to select nothing
      this.value = null;
      this.label = '';
    } else if (Array.isArray(result)) {
      // If an array, user selected multiple items
      this.value = result;
      this.label = result.map(this._itemSelectorConfig.getLabel).join(', ');
    } else if (!Array.isArray(result)) {
      // If not an array, user selected one item
      this.value = result;
      this.label = this._itemSelectorConfig.getLabel(result);
    }

    return true;
  }
}
