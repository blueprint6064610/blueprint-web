import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IQueryRequest } from '../interface/query-request.interface';

@Injectable({
  providedIn: 'root'
})
export class QueryService {
  getParams(query: IQueryRequest): HttpParams {
    let params = new HttpParams();

    // Handle pagination
    if (query.pagination?.page) {
      params = params.append('page', query.pagination.page);
    }

    if (query.pagination?.pageSize) {
      params = params.append('pageSize', query.pagination.pageSize);
    }

    // Handle filters
    if (query.filter) {
      Object.entries<string>(query.filter).forEach(([key, value]) => {
        if (value === null || value === undefined || value === '') return;
        params = params.append(key, value);
      });
    }
    
    return params;
  }
}
