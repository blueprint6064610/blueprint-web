import { Injector } from "@angular/core";
import { RelationDataSource } from "../data-source/relation.data-source";
import { IGridConfig } from "../interface/grid-config.interface";
import { IRelation } from "../interface/relation.interface";
import { RelationService } from "../service/relation.service";

/**
 * Configuration class for a relation grid.
 */
export class RelationGridConfig implements IGridConfig<IRelation> {
  /**
   * @inheritdoc
   */
  columns = [
    { field: 'name', headerText: 'Name' },
    { field: 'description', headerText: 'Description' },
    { field: 'relationType', headerText: 'Relation Type' }
  ];

  /**
   * @inheritdoc
   */
  dataSource = new RelationDataSource(this.injector.get(RelationService), this.schematicId);

  /**
   * @inheritdoc
   */
  select = true;

  /**
   * RelationGridConfig constructor.
   * @param injector The injector.
   * @param schematicId The schematic id.
   */
  constructor(
    private readonly injector: Injector,
    private readonly schematicId: string
  ) { }

  /**
   * @inheritdoc
   */
  getId(row: IRelation): any {
    return row.id;
  }
}
