import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ISchematic } from '../interface/schematic.interface';
import { SchematicService } from '../service/schematic.service';

/**
 * Resolves a schematic.
 */
@Injectable({
  providedIn: 'root'
})
export class SchematicResolver implements Resolve<ISchematic> {
  /**
   * SchematicResolver constructor.
   */
  constructor(
    private readonly schematicService: SchematicService
  ) { }

  /**
   * @inheritdoc
   */
  resolve(route: ActivatedRouteSnapshot): Observable<ISchematic> {
    const id = route.paramMap.get('schematicId');
    if (!id) { throw new Error(':schematicId param not found'); }
    return this.schematicService.findById(id);
  }
}
