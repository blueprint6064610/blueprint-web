import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCreateContainerComponent } from './user-create-container.component';

describe('UserCreateContainerComponent', () => {
  let component: UserCreateContainerComponent;
  let fixture: ComponentFixture<UserCreateContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserCreateContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCreateContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
