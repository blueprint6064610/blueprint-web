import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router} from '@angular/router';
import { IAuthToken } from '../interface/auth-token.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(
    private readonly router: Router
  ) { }

  canActivate(): Promise<boolean> {
    return this.isTokenValid();
  }

  canActivateChild(): Promise<boolean> {
    return this.isTokenValid();
  }

  private async isTokenValid(): Promise<boolean> {
    const token = this.getToken();
    const unixSeconds = new Date().getTime() / 1000;
    const isValid = !!token && token.exp >= unixSeconds;

    if (isValid) {
      return true;
    } else {
      await this.router.navigate(['login']);
      return false;
    }
  }
  
  private getToken(): IAuthToken | null {
    const jwt = localStorage.getItem('token');
    if (jwt) {
      return JSON.parse(atob(jwt.split('.')[1]));
    } else {
      return null;
    }
  }
}
