import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchematicGridContainerComponent } from './schematic-grid-container.component';

describe('SchematicGridContainerComponent', () => {
  let component: SchematicGridContainerComponent;
  let fixture: ComponentFixture<SchematicGridContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchematicGridContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchematicGridContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
