import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomUpdateContainerComponent } from './custom-update-container.component';

describe('CustomUpdateContainerComponent', () => {
  let component: CustomUpdateContainerComponent;
  let fixture: ComponentFixture<CustomUpdateContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomUpdateContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomUpdateContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
