import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ICustomEntity } from '../interface/custom-entity.interface';
import { IQueryRequest } from '../interface/query-request.interface';
import { IQueryResponse } from '../interface/query-response.interface';
import { QueryService } from './query.service';

@Injectable({
  providedIn: 'root'
})
export class CustomService {
  private static readonly API_URL = `${environment.apiBase}/custom`;

  constructor(
    private readonly httpClient: HttpClient,
    private readonly queryService: QueryService
  ) { }

  create(schematicId: string, request: Record<string, any>): Observable<void> {
    const url = `${CustomService.API_URL}/${schematicId}`;
    return this.httpClient.post<void>(url, request);
  }

  delete(schematicId: string, id: string): Observable<void> {
    const url = `${CustomService.API_URL}/${schematicId}/${id}`;
    return this.httpClient.delete<void>(url);
  }

  find(schematicId: string, query: IQueryRequest): Observable<IQueryResponse<ICustomEntity>> {
    const url = `${CustomService.API_URL}/${schematicId}`;
    const params = this.queryService.getParams(query);
    return this.httpClient.get<IQueryResponse<ICustomEntity>>(url, { params });
  }

  findById(schematicId: string, id: string): Observable<ICustomEntity> {
    const url = `${CustomService.API_URL}/${schematicId}/${id}`;
    return this.httpClient.get<ICustomEntity>(url);
  }

  update(schematicId: string, entityId: string, request: Record<string, any>): Observable<void> {
    const url = `${CustomService.API_URL}/${schematicId}/${entityId}`;
    return this.httpClient.put<void>(url, request);
  }
}
