import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IRelationCreateRequest } from 'src/app/common/interface/relation-create-request.interface';
import { RelationType } from 'src/app/common/interface/relation-type.interface';
import { ISchematic } from 'src/app/common/interface/schematic.interface';
import { RelationService } from 'src/app/common/service/relation.service';
import { RelationFormComponent } from '../relation-form/relation-form.component';

export interface IRelationCreateDialogData {
  schematicId: string;
}

@Component({
  selector: 'app-relation-create-dialog',
  templateUrl: './relation-create-dialog.component.html',
  styleUrls: ['./relation-create-dialog.component.scss']
})
export class RelationCreateDialogComponent {
  @ViewChild(RelationFormComponent) relationForm!: RelationFormComponent;

  constructor(
    @Inject(MAT_DIALOG_DATA) private readonly data: IRelationCreateDialogData,
    private readonly matDialogRef: MatDialogRef<RelationCreateDialogComponent>,
    private readonly relationService: RelationService
  ) { }

  get form(): FormGroup {
    return this.relationForm.relationForm;
  }

  get name(): string {
    return this.form.get('name')?.value;
  }

  get description(): string {
    return this.form.get('description')?.value;
  }

  get schematic(): ISchematic {
    return this.form.get('schematic')?.value;
  }

  get relationType(): RelationType {
    return this.form.get('relationType')?.value;
  }

  create(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    const lhsSchematicId = this.data.schematicId;
    const request: IRelationCreateRequest = {
      name: this.name,
      description: this.description,
      lhsSchematicId,
      rhsSchematicId: this.schematic.id,
      relationType: this.relationType
    };

    this.relationService.create(request).subscribe(() => {
      this.matDialogRef.close(true);
    });
  }
}
