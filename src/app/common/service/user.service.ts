import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IQueryRequest } from '../interface/query-request.interface';
import { IQueryResponse } from '../interface/query-response.interface';
import { IUserCreateRequest } from '../interface/user-create-request.interface';
import { IUserUpdateRequest } from '../interface/user-update-request.interface';
import { IUser } from '../interface/user.interface';
import { QueryService } from './query.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private static readonly API_URL = `${environment.apiBase}/users`;

  constructor(
    private readonly httpClient: HttpClient,
    private readonly queryService: QueryService
  ) { }

  find(query: IQueryRequest): Observable<IQueryResponse<IUser>> {
    const url = UserService.API_URL;
    const params = this.queryService.getParams(query);
    return this.httpClient.get<IQueryResponse<IUser>>(url, {params});
  }

  findById(id: string): Observable<IUser> {
    const url = `${UserService.API_URL}/${id}`;
    return this.httpClient.get<IUser>(url);
  }

  create(request: IUserCreateRequest): Observable<void> {
    const url = UserService.API_URL;
    return this.httpClient.post<void>(url, request);
  }

  update(id: string, request: IUserUpdateRequest): Observable<void> {
    const url = `${UserService.API_URL}/${id}`;
    return this.httpClient.put<void>(url, request);
  }

  delete(id: string): Observable<void> {
    const url = `${UserService.API_URL}/${id}`;
    return this.httpClient.delete<void>(url);
  }
}
