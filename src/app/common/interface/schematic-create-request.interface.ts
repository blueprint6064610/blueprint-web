/**
 * Create a schematic request.
 */
export interface ISchematicCreateRequest {
  /**
   * The schematic name.
   */
  name: string;

  /**
   * The schematic description.
   */
  description: string;
}
