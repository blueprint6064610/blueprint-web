import { Component, Injector, Input, OnInit } from '@angular/core';
import { RelationGridConfig } from 'src/app/common/grid-config/relation.grid-config';
import { IGridConfig } from 'src/app/common/interface/grid-config.interface';
import { IRelation } from 'src/app/common/interface/relation.interface';

/**
 * Relation grid component.
 */
@Component({
  selector: 'app-relation-grid',
  templateUrl: './relation-grid.component.html',
  styleUrls: ['./relation-grid.component.scss']
})
export class RelationGridComponent implements OnInit {
  /**
   * The schematic to fetch relations for.
   */
  @Input() schematicId!: string;
  
  /**
   * The grid config.
   */
  gridConfig!: IGridConfig<IRelation>;

  /**
   * The selected ids.
   */
  private _selected: IRelation[] = [];

  /**
   * RelationGridComponent constructor.
   */
  constructor(
    private readonly injector: Injector
  ) { }

  /**
   * @inheritdoc
   */
  ngOnInit(): void {
    this.gridConfig = new RelationGridConfig(this.injector, this.schematicId);
  }

  /**
   * Gets the selected ids.
   */
  get selected(): IRelation[] {
    return this._selected;
  }

  /**
   * Handles when the grids selected rows change.
   * @param event The selected ids.
   */
  onSelectedChange(event: IRelation[]): void {
    this._selected = event;
  }
}
