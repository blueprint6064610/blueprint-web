import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/common/service/user.service';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
import { UserGridComponent } from '../user-grid/user-grid.component';

@Component({
  selector: 'app-user-grid-container',
  templateUrl: './user-grid-container.component.html',
  styleUrls: ['./user-grid-container.component.scss']
})
export class UserGridContainerComponent {
  @ViewChild('grid') grid?: UserGridComponent;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly matDialog: MatDialog,
    private readonly router: Router,
    private readonly userService: UserService
  ) { }

  get isOneSelected(): boolean {
    return this.grid?.selected.length === 1;
  }

  async create(): Promise<void> {
    await this.router.navigate(['create'], {relativeTo: this.activatedRoute});
  }

  async update(): Promise<void> {
    const id: string | undefined = this.grid?.selected[0]?.id;
    await this.router.navigate([id], {relativeTo: this.activatedRoute});
  }

  delete(): void {
    const user = this.grid?.selected[0];
    if (!user) { return; }

    this.matDialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Delete User',
        message: 'Are you sure you want to delete this user?',
        action: 'Delete',
        theme: 'warn'
      }
    }).afterClosed().subscribe((confirm: boolean) => {
      if (!confirm) { return; }
      this.userService.delete(user.id).subscribe(() => {
        window.location.reload();
      });
    });
  }
}
