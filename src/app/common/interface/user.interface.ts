/**
 * A system user.
 */
export interface IUser {
  /**
   * The user id.
   */
  id: string;

  /**
   * The users first name.
   */
  firstName: string;

  /**
   * The users last name.
   */
  lastName: string;

  /**
   * The users email address.
   */
  email: string;
}
